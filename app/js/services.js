var baseUrl = 'http://beta.app.showme.uz';
// var baseUrl = 'http://192.168.1.188:8001';

if (window.location.host.match(/cabinet\.showme\.uz/)) {
  baseUrl = 'http://app.showme.uz';
}

var apiUrl = baseUrl + '/v2/cabinet/';

var adminServices = angular.module('app.services', [])

.factory('LoginService', function ($http, $q, $sessionStorage) {
  return {
    login: function (email, password) {
      var def = $q.defer();

      var data = {
        email: email,
        password: password
      };

      $http.post(apiUrl + 'auth', data).
        then(function (response) {
          def.resolve(response.data);
        }, function (response) {
          def.reject(response.data);
        });

      return def.promise;
    }
  };
})

.factory('TimelineService', function ($http, $q) {
  return {
    getTimeline: function (time) {
      var def = $q.defer();

      if (!time) {
        time = 0;
      }

      var url = apiUrl + 'timeline?time=' + time;

      $http.get(url)
        .success(function (data, status, headers, config) {
          def.resolve(data);
        })
        .error(function (data, status, headers, config) {
          def.reject();
        });

      return def.promise;
    }
  }
})

.factory('FilesService', function ($http, $q, $sessionStorage, Upload) {
  var upload;

  return {
    update: function () {
      $http.get(apiUrl + 'files');
    },
    uploadPhoto: function (file, type) {
      var def = $q.defer();

      upload = Upload.upload({
        url: apiUrl + 'files/photo',
        data: {
          'file-type': type,
          'photo': file
        }
      });

      upload.then(function (resp) {
        def.resolve(resp.data);
      }, function (resp) {
        def.reject();
      }, function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        def.notify(progressPercentage);
      });

      return def.promise;
    },
    uploadAudio: function (file) {
      var def = $q.defer();

      upload = Upload.upload({
        url: apiUrl + 'files/audio',
        data: {
          audio: file
        }
      });

      upload.then(function (resp) {
        def.resolve(resp.data);
      }, function (resp) {
        def.reject();
      }, function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        def.notify(progressPercentage);
      });

      return def.promise;
    },
    uploadVideo: function (file) {
      var def = $q.defer();

      upload = Upload.upload({
        url: apiUrl + 'files/video',
        data: {
          video: file
        }
      });

      upload.then(function (resp) {
        def.resolve(resp.data);
      }, function (resp) {
        def.reject();
      }, function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        def.notify(progressPercentage);
      });

      return def.promise;
    },

    abortUpload: function() {
      upload.abort();
    }
  };
})

.factory('PostsService', function ($http, $q) {
  return {
    savePost: function (post) {
      var def = $q.defer();
      var resp = $http.post(apiUrl + 'posts', post);

      resp.success(function (data, status, headers, config) {
        def.resolve(data);
      })
      .error(function (data, status, headers, config) {
        def.reject(data);
      });

      return def.promise;
    },

    deletePost: function(postID) {
      var def = $q.defer();

      $http.delete(apiUrl + 'posts/' + postID)
        .success(function (data, status, headers, config) {
          def.resolve(data);
        })
        .error(function (data, status, headers, config) {
          def.reject(data);
        });

      return def.promise;
    }
  };
});
