angular.module('app', [
  'ngMessages',
  'ui.router',
  'ui.bootstrap',
  'ngStorage',
  'ngFileUpload',
  'angularMoment',
  'angularSpinner',
  'infinite-scroll',
  'app.controllers',
  'app.services'
])

.factory('RequestsErrorHandler', function($q, $rootScope) {
  return {
    responseError: function(rejection) {
      // console.log(rejection);
      if (rejection.status == 401) {
        $rootScope.logout();
      }

      return $q.reject(rejection);
    }
  };
})

// TODO: solution for safe video URLs
.config(function($sceProvider) {
  // Completely disable SCE.  For demonstration purposes only!
  // Do not use in new projects.
  $sceProvider.enabled(false);
})

.config(function ($provide, $httpProvider) {
  $httpProvider.interceptors.push('RequestsErrorHandler');
})

.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })

    .state('dashboard', {
      templateUrl: 'templates/dashboard.html',
      abstract: true,
      requiresLogin: true
    })

    .state('timeline', {
      parent: 'dashboard',
      url: '/timeline',
      templateUrl: 'templates/timeline.html',
      controller: 'TimelineCtrl',
      requiresLogin: true
    });

  $urlRouterProvider.otherwise('/login');
})

.run(function ($rootScope, $state, $http, $sessionStorage, amMoment) {
  amMoment.changeLocale('ru');

  var token = $sessionStorage.token;

  if (token) {
    $rootScope.isLoggedIn = true;
    $rootScope.user = $sessionStorage.user;
    $http.defaults.headers.common.Authorization = 'Token ' + token;
  }

  $rootScope.logout = function () {
    $rootScope.isLoggedIn = false;
    $http.defaults.headers.common.Authorization = '';
    delete $sessionStorage.token;
    delete $sessionStorage.user;
    $state.go('login');
  };

  $rootScope.login = function (data) {
    $sessionStorage.token = data.token;
    $sessionStorage.user = data.user;
    $rootScope.isLoggedIn = true;
    $rootScope.user = data.user;
    $http.defaults.headers.common.Authorization = 'Token ' + data.token;
    $state.go('timeline');
  };

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
    var requireLogin = toState.requiresLogin || false;
    var token = $sessionStorage.token || false;

    if (requireLogin && !token) {
      event.preventDefault();
      var result = $state.go('login');
    } else {
      $rootScope.isLoggedIn = true;
    }
  });
});
