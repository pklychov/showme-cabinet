var adminControllers = angular.module('app.controllers', [])

.controller('LoginCtrl',
  function ($scope, $rootScope, $state, $sce, $sessionStorage, $uibModal, LoginService) {
    var token = $sessionStorage.token;
    $scope.email = '';
    $scope.password = '';

    if (token) {
      $rootScope.isLoggedIn = true;
      $state.go('timeline');
    }

    $scope.login = function (email, password) {
      LoginService.login(email, password).then(
        function (data) {
          $rootScope.login(data);
        },
        function (data) {
          $scope.title = data.title;
          $scope.description = $sce.trustAsHtml(data.description);

          var modalInstance = $uibModal.open({
            templateUrl: 'error-modal.html',
            scope: $scope
          });
        }
      );
    };
  }
)

.controller('NewPostModalInstanceCtrl',
  function ($scope, $uibModal, $uibModalInstance, $state, PostsService, FilesService, postType) {
    $scope.isPostSaving = false;
    $scope.showErrorMessage = false;
    $scope.showSuccessMessage = false;
    $scope.errorMessage = "";
    $scope.successMessage = "";

    $scope.text = "";
    $scope.photo = {};
    $scope.audio = {};
    $scope.video = {};
    $scope.cover = '';

    $scope.isFileUploading = false;
    $scope.fileUploadPercent = 0;
    $scope.audio.isAudioUploading = false;
    $scope.audio.fileUploadPercent = 0;

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
      FilesService.abortUpload();
    };

    $scope.closeAlert = function () {
      $scope.showErrorMessage = false;
    };

    $scope.uploadPhoto = function (file, photoType) {
      if ($scope.isFileUploading) {
        $scope.errorMessage = "Пожалуйста, подождите загрузки файла.";
        $scope.showErrorMessage = true;
        return;
      }

      if (!file) {
        return;
      }

      if (!photoType) {
        photoType = 'photo';
      }

      $scope.isFileUploading = true;
      $scope.fileUploadPercent = 0;

      FilesService.uploadPhoto(file, photoType).then(
        function (data) {
          $scope.isFileUploading = false;
          $scope.errorMessage = 'Файл: \"' + file.name + '\"' + ' загружен.';
          $scope.showErrorMessage = true;
          $scope.showSuccessMessage = false;

          if (photoType == 'photo') {
            $scope.photo = data;
          } else if (photoType == 'artwork') {
            $scope.audio.artwork = data;
          }
        },
        function (data) {
          $scope.isFileUploading = false;
        },
        function (progressPercentage) {
          $scope.fileUploadPercent = progressPercentage;
        }
      );
    };

    $scope.uploadAudio = function (file) {
      if ($scope.isFileUploading) {
        return;
      }

      if (!file) {
        return;
      }

      $scope.isFileUploading = true;
      $scope.fileUploadPercent = 0;
      FilesService.uploadAudio(file).then(
        function (data) {
          $scope.isFileUploading = false;
          $scope.audio.id = data.id;
          $scope.audio.url = data.url;
          $scope.successMessage = 'Файл: \"' + file.name + '\"' + ' успешно загружен.';
          $scope.showSuccessMessage = true;
        },
        function (data) {
          $scope.isFileUploading = false;
        },
        function (progressPercentage) {
          $scope.fileUploadPercent = progressPercentage;
        }
      );
    };

    $scope.uploadVideo = function (file) {

      if ($scope.isFileUploading) {
        return;
      }

      if (!file) {
        return;
      }

      $scope.isFileUploading = true;
      $scope.fileUploadPercent = 0;

      FilesService.uploadVideo(file).then(
        function (data) {
          $scope.isFileUploading = false;
          $scope.video.id = data.id;
          $scope.video.url = data.url;
          $scope.video.cover = data.cover_id;
          $scope.successMessage = "Файл успешно загружен.";
          $scope.showSuccessMessage = true;
        },
        function (data) {
          $scope.isFileUploading = false;
        },
        function (progressPercentage) {
          $scope.fileUploadPercent = progressPercentage;
        }
      );
    };

    $scope.publish = function () {

      if ($scope.isFileUploading || $scope.isPostSaving) {
        $scope.errorMessage = "Пожалуйста, подождите загрузки файла.";
        $scope.showErrorMessage = true;
        return;
      }

      if (!$scope.text ) {
        $scope.errorMessage = "Введите текст перед сохранением!";
        $scope.showErrorMessage = true;
        return;
      }

      var post = {
        type: postType,
        text: $scope.text
      };

      if (postType == 'text') {
        $scope.isPostSaving = true;
      }

      if (postType == 'photo') {

        if (!$scope.photo.id) {
          $scope.errorMessage = "Загрузите фото.";
          $scope.showErrorMessage = true;
          return;
        }

        post.photo = $scope.photo.id;

        $scope.isPostSaving = true;
      }

      if (postType == 'audio') {
        if (!$scope.audio.id) {
          $scope.errorMessage = "Загрузите аудио.";
          $scope.showErrorMessage = true;
          return;
        }

        if (!$scope.audio.artwork) {
          $scope.errorMessage = "Пожалуйста, загрузите аудио артворк.";
          $scope.showErrorMessage = true;
          return;
        }

        var artwork = null;
        if ($scope.audio.artwork) {
          artwork = $scope.audio.artwork.id;
        }

        post.title = $scope.audio.title;
        post.artist = $scope.audio.artist;
        post.artwork = artwork;
        post.audio = $scope.audio.id;

        $scope.isPostSaving = true;
      }

      if (postType == 'video') {
        if (!$scope.video.id) {
          $scope.errorMessage = "Загрузите видео.";
          $scope.showErrorMessage = true;
          return;
        }

        post.video = $scope.video.id;
        post.cover = $scope.video.cover;

        $scope.isPostSaving = true;
      }

      PostsService.savePost(post).then(
        function (data) {
          $scope.isPostSaving = false;
          $uibModalInstance.dismiss('cancel');

          if (postType == 'audio' || postType == 'video') {
            var modalInstance = $uibModal.open({
              templateUrl: 'templates/modals/audio-video-success.html'
            });
            modalInstance.result.then(
              function () {
                $state.go('timeline', {}, {reload: true});
              },
              function () {
                $state.go('timeline', {}, {reload: true});
              }
            );
          } else {
            $state.go('timeline', {}, {reload: true});
          }
        },
        function (data) {
          $scope.isPostSaving = false;
          $scope.errorMessage = "Произошла ошибка при сохранения поста.";
          $scope.showErrorMessage = true;
        }
      );

    };
  }
)

.controller('TimelineCtrl',
  function ($scope, $rootScope, $state, $stateParams, $uibModal, $http, TimelineService, PostsService) {
    $scope.showSpinner = false;
    $scope.time = 0;
    $scope.timeline = [];

    var timelineIsEnded = false;

    function loadTimeline() {
      if (timelineIsEnded) {
        return;
      }

      $scope.showSpinner = true;
      TimelineService.getTimeline($scope.time).then(
        function (data) {
          $scope.showSpinner = false;

          if (data.results.length === 0) {
            timelineIsEnded = true;
          } else {
            $scope.time = data.results[data.results.length - 1].time;
          }

          [].push.apply($scope.timeline, data.results);
        }
      );
    }

    loadTimeline();
    $scope.nextPage = function () {
      if ($scope.showSpinner) {
        return;
      }
      loadTimeline();
    };

    var postType = $stateParams.postType;
    if (postType) {
      console.log(postType);
    } else {
    }

    $rootScope.openModal = function (postType) {
      var templateName;
      if (postType == 'text') {
        templateName = 'text.html';
      } else if (postType == 'photo') {
        templateName = 'photo.html';
      } else if (postType == 'audio') {
        templateName = 'audio.html';
      } else if (postType == 'video') {
        templateName = 'video.html';
      }

      var modalInstance = $uibModal.open({
        templateUrl: 'templates/modals/' + templateName,
        controller: 'NewPostModalInstanceCtrl',
        resolve: {
          postType: function () {
            return postType;
          }
        }
      });
    };

    $scope.delete = function(postID) {
      var alert = $('#alert-modal');
      alert.modal('show');

      $('#delete-post', alert).on('click', function() {
        $('#alert-modal').modal('hide');
        $('.modal-backdrop', 'body').hide();

        PostsService.deletePost(postID).then(
          function () {
            $state.go('timeline', {}, {reload: true});
          },
          function () {
            console.log( 'Error: cannot delete the post.' );
          }
        );
      });

    };
  }
)