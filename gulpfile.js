'use strict';

var gulp 		= require('gulp'),
		sass 		= require('gulp-sass'),
		connect = require('gulp-connect');

gulp.task('serve', function() {
	connect.server({
		port: 8000,
		root: './app',
		livereload: true
	});
});

gulp.task('html', function() {
	gulp.src('./app/**/*.html')
		.pipe(connect.reload());
});

gulp.task('css', function() {
	gulp.src('./app/**/*.css')
		.pipe(connect.reload());
});

gulp.task('js', function() {
	gulp.src('./app/**/*.js')
		.pipe(connect.reload());
});

gulp.task('sass', function () {
  return gulp.src('./app/sass/**/*.sass')
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest('./app/css'));
});

gulp.task('watcher', function() {
	gulp.watch('./app/**/*.html', ['html']);
	gulp.watch('./app/**/*.css', ['css']);
	gulp.watch('./app/**/*.js', ['js']);
	gulp.watch('./app/sass/**/*.sass', ['sass']);
});

gulp.task('default', ['serve', 'watcher']);
